# Python 리스트 데이터 구조: 생성과 액세스

## <a name="intro"></a> 소개
이 포스팅에서는 항목의 정렬된 컬렉션인 리스트를 만들고 액세스하는 방법을 설명한다. 리스트는 단일 변수에 여러 값을 저장하고 조작할 수 있기 때문에 Python에서 가장 일반적으로 사용되는 데이터 구조 중 하나이다.

이 튜토리얼이 끝나면 리스트를 만들 수 있다.

- 다양한 방법으로 리스트 만들기
- 인덱싱과 슬라이싱을 사용하여 리스트 요소 액세스하기
- 리스트 메서드와 연산으로 리스트 수정과 조작하기

시작하기 전에 컴퓨터에 Python과 원하는 코드 편집기가 설치되어 있는지 확인하세요. 이 포스팅의 코드 예제를 실행하기 위해 [Repl.it](https://replit.com/)과 같은 온라인 Python 인터프리터를 사용할 수도 있다.

Python 리스트에 대해 알아볼 준비가 되었나요? 시작합시다!

## <a name="sec_02"></a> Python에서 리스트란?
리스트는 단일 변수에 서로 다른 타입의 여러 값을 저장할 수 있는 Python의 데이터 구조이다. 리스트는 시퀀스의 한 예로, 리스트에 있는 요소의 순서가 유지되고 위치나 인덱스로 요소를 액세스할 수 있다는 것을 의미한다. 리스트는 또한 변경 가능한 데이터 구조의 한 예로서, 리스트를 만든 후 리스트의 요소를 변경할 수 있다.

리스트에는 숫자, 문자열, 부울 또는 다른 리스트 등 모든 타입의 데이터를 포함할 수 있다. 같은 리스트에 서로 다른 타입의 데이터를 혼합할 수도 있다. 예를 들어 숫자, 문자열, 부울, 다른 리스트가 포함된 리스트를 만들 수 있다.

```python
# Create a list with different types of data
my_list = [42, "Hello", True, [1, 2, 3]]

# Print the list
print(my_list)
```

코드를 실행한 출력은 다음과 같다.

```
[42, 'Hello', True, [1, 2, 3]]
```

보다시피 리스트는 대괄호`[` `]`로 묶여 있고 요소는 쉼표(`,`)로 구분되어 있다. 리스트에는 0개부터 컴퓨터 메모리가 수용할 수 있는 갯수까지 원하는 만큼의 요소를 포함할 수 있다. 빈 리스트는 요소가 없는 리스트로 `[]`으로 표시한다.

```python
# Create an empty list
empty_list = []

# Print the list
print(empty_list)
```

이 코드의 출력은 다음과 같다.

```
[]
```

리스트는 요소를 추가, 제거, 정렬 또는 검색하는 등 다양한 작업을 수행할 수 있는 많은 메서드와 연산을 제공하므로 Python에서 데이터를 저장하고 조작하는 데 매우 유용하다. 다음 섹션에서 이러한 메서드와 연산을 살펴보겠다.

하지만 먼저 다양한 방법을 사용하여 Python에서 리스트를 만드는 방법을 살펴보겠다.

## <a name="sec_03"></a> Python에서 리스트를 만드는 방법
필요와 선호도에 따라 Python에서 리스트를 만드는 여러 가지 방법이 있다. 이 섹션에서는 다음과 같이 가장 일반적인 방법을 보인다.

- 대괄호 `[` `]` 사용
- `list()` 함수 사용
- 리스트 컴프리헨션 사용
- `split()` 메서드 사용

각 메서드가 어떻게 작동하는지 자세히 살펴보자.

## <a name="sec_04"></a> Python에서 리스트 원소 액세스 방법
Python에서 리스트를 만든 다음에는 인쇄, 수정 또는 계산 수행 등 다양한 목적으로 리스트의 요소를 액세스하고자 할 수 있다. 리스트의 요소를 액세스하려면 리스트의 각 요소를 참조할 수 있는 고유한 위치 또는 인덱스가 있다는 의미의 인덱싱 개념을 사용해야 한다.

Python에서 인덱싱은 0부터 시작하며, 이는 리스트의 첫 번째 요소는 인덱스 0, 두 번째 요소는 인덱스 1을 갖는다는 의미이다. 예를 들어 4개의 요소가 포함된 `fruits`이라는 리스트가 있는 경우 다음과 같이 인덱스로 요소를 액세스할 수 있다.

```python
# Create a list of fruits
fruits = ["apple", "banana", "cherry", "date"]

# Print the first element of the list
print(fruits[0]) # Output: apple

# Print the second element of the list
print(fruits[1]) # Output: banana

# Print the third element of the list
print(fruits[2]) # Output: cherry

# Print the fourth element of the list
print(fruits[3]) # Output: date
```

네거티브 인덱싱을 사용하여 리스트의 요소에 끝부터 액세스할 수도 있다. 음수 인덱싱은 -1부터 시작하므로 리스트의 마지막 요소의 인덱스는 -1, 두 번째 마지막 요소의 인덱스는 -2가 된다. 예를 들어 다음과 같이 음수 인덱스로 `fruits` 리스트의 요소에 액세스할 수 있다.

```python
# Print the last element of the list
print(fruits[-1]) # Output: date

# Print the second last element of the list
print(fruits[-2]) # Output: cherry

# Print the third last element of the list
print(fruits[-3]) # Output: banana

# Print the fourth last element of the list
print(fruits[-4]) # Output: apple
```

인덱싱을 사용하면 리스트의 단일 요소에 액세스할 수 있지만, 리스트의 범위 또는 하위 집합에 액세스하려면 어떻게 해야 할까? 이를 위해서는 슬라이싱이라는 개념을 사용해야 하는데, 이는 시작 인덱스와 끝 인덱스를 지정하여 리스트의 슬라이스를 얻을 수 있다는 뜻이다. 슬라이싱의 구문은 `list[start:end]`이며, 여기서 `start`는 슬라이스의 첫 번째 요소의 인덱스이고 `end`는 슬라이스의 마지막 요소 뒤의 요소의 인덱스이다. 예를 들어 두 번째와 세 번째 요소가 포함된 `fruits` 리스트의 슬라이스를 얻으려면 다음과 같이 슬라이싱을 사용할 수 있다.

```python
# Get a slice of the list from index 1 to index 3 (exclusive)
print(fruits[1:3]) # Output: ['banana', 'cherry']
```

슬라이스에는 끝 인덱스의 요소가 포함되지 않으므로 `fruits[1:3]`은 인덱스 1과 2의 요소는 반환하지만 인덱스 3의 요소는 반환하지 않는다는 점에 유의하세요. 시작 인덱스나 끝 인덱스를 생략하여 리스트의 시작 또는 끝에서 슬라이스를 가져올 수도 있다. 예를 들어 처음 세 개의 요소가 포함된 `fruits` 리스트의 슬라이스를 얻으려면 다음과 같이 슬라이싱을 사용할 수 있다.

```python
# Get a slice of the list from the beginning to index 3 (exclusive)
print(fruits[:3]) # Output: ['apple', 'banana', 'cherry']
```

마찬가지로 마지막 두 요소가 포함된 `fruits` 리스트의 슬라이스를 얻으려면 다음과 같이 슬라이싱을 사용할 수 있다.

```python
# Get a slice of the list from index -2 to the end
print(fruits[-2:]) # Output: ['cherry', 'date']
```

슬라이싱을 사용하면 리스트의 다양한 요소를 액세스할 수 있지만 리스트의 n번 마다의 모든 요소를 액세스하거나 그 사이에 있는 일부 요소를 건너뛰려면 어떻게 해야 할까? 이를 위해서는 **보폭(stride)**라는 개념을 사용해야 한다. 이는 리스트의 n번째 건너 요소를 모두 가져오기 위해 단계 크기 또는 간격을 지정할 수 있다는 뜻이다. 보폭의 구문은 `list[start:end:step]`이며, 여기서 `start`과 `end`는 슬라이싱에서와 동일하고 `step`은 요소 사이의 간격이다. 예를 들어 `fruits` 리스트의 두 번째 마다의 요소를 가져오려면 다음과 같이 보폭를 사용할 수 있다.

```python
# Get every second element of the list
print(fruits[::2]) # Output: ['apple', 'cherry']
```

슬라이싱과 보폭을 결합하여 특정 간격으로 리스트의 슬라이스를 가져올 수도 있다. 예를 들어 단계 크기가 `2`인 `fruits` 리스트의 처음 세 요소를 가져오려면 다음과 같이 슬라이싱과 보폭을 사용할 수 있다.

```python
# Get the first three elements of the list with a step size of 2
print(fruits[:3:2]) # Output: ['apple', 'cherry']
```

인덱싱, 슬라이싱 및 보폭은 Python에서 리스트의 요소를 액세스할 수 있는 강력한 도구이다. 이러한 도구를 사용하여 리스트 요소를 인쇄, 수정 또는 계산을 수행하는 등 다양한 작업을 수행할 수 있다. 다음 섹션에서는 Python에서 리스트를 수정하고 조작하는 데 사용할 수 있는 메서드와 연산에 대해 알아보겠다.

## <a name="sec_05"></a> Python의 리스트 메서드와 연산
이 섹션에서는 Python에서 리스트를 수정하고 조작하는 데 사용할 수 있는 메서드와 연산에 대해 살펴본다. 메서드는 리스트와 같은 특정 객체와 연관된 함수이며 점 표기법을 사용하여 호출할 수 있다. 예를 들어 `list.append(x)`는 리스트 끝에 요소 `x`를 추가하는 메서드이다. 연산자는 리스트와 함께 하나 이상의 피연산자에 대해 어떤 작업을 수행하는 기호 또는 키워드이다. 예를 들어 `list + list2`는 두 개의 리스트를 연결하는 연산이다.

Python에서 가장 일반적인 리스트 메서드와 연산은 다음과 같다.

- `append()`, `insert()` 또는 `extend()` 메서드를 사용하여 리스트에 요소 추가
- `remove()`, `pop()` 또는 `del` 연산을 사용하여 리스트에서 요소 제거
- `sort()` 또는 `sorted()` 메서드를 사용하여 리스트 정렬
- `reverse()` 또는 `reversed()` 메서드를 사용하여 리스트 반전
- `count()` 메서드를 사용하여 리스트에서 요소의 발생 횟수 계산
- `index()` 메서드를 사용하여 리스트에서 요소의 인덱스 찾기
- 연산자를 사용하여 요소가 리스트에 있는지 여부 확인
- `for` 루프 또는 `list comprehesion`을 사용하여 리스트의 요소 반복

이러한 각 메서드와 연산이 어떻게 작동하는지 자세히 살펴보자.

## <a name="conclusion"></a> 맺는말
이 포스팅에서는 Python에서 항목의 정렬된 컬렉션인 리스트를 만들고 액세스하는 방법을 배웠다. 또한 Python에서 리스트를 수정하고 조작할 수 있는 몇 가지 메서드와 연산을 사용하는 방법도 배웠다. 리스트는 다음과 같은 많은 이점을 제공하기 때문에 Python에서 가장 일반적으로 사용되는 데이터 구조 중 하나이다.

- 단일 변수에 서로 다른 타입의 여러 값을 저장할 수 있다.
= 변경 가능하므로 생성 후 리스트의 요소를 변경할 수 있다.
- 반복 가능하므로 for 루프 또는 list comprehension을 사용하여 리스트의 요소를 반복할 수 있다.
- 요소 추가, 제거, 정렬 또는 검색과 같은 다양한 작업을 수행할 수 있는 많은 내장 메서드와 연산이 있다.

리스트는 Python에서 데이터를 저장하고 조작하는 데 매우 유용하며 다음과 같은 다양한 용도로 사용할 수 있다.

- 성적, 점수 또는 가격 등의 수치 데이터 저장 및 처리
- 단어, 문장 또는 단락과 같은 텍스트 데이터 저장 및 처리
- 부울, 조건 또는 플래그와 같은 논리적 데이터 저장 및 처리
- 리스트, 행렬 또는 그래프 리스트 같은 중첩 데이터 저장 및 처리

이 포스팅이 Python의 리스트에 대한 기본적인 이해를 돕고 학습한 것을 자신의 프로젝트에 적용할 수 있기를 바란다.
