# Python 리스트 데이터 구조: 생성과 액세스 <sup>[1](#footnote_1)</sup>

> <font size="3">정렬된 컬렉션인 리스트를 만들고 액세스하는 방법을 알아본다.</font>

## 목차

1. [소개](./list-data-structure.md#intro)
1. [Python에서 리스트란?](./list-data-structure.md#sec_02)
1. [Python에서 리스트를 만드는 방법](./list-data-structure.md#sec_03)
1. [Python에서 리스트 원소 액세스 방법](./list-data-structure.md#sec_04)
1. [Python의 리스트 메서드와 연산](./list-data-structure.md#sec_05)
1. [맺는말](./list-data-structure.md#conclusion)


<a name="footnote_1">1</a>: [Python Tutorial 8 — Python List Data Structure: Creation, Access](https://levelup.gitconnected.com/python-tutorial-8-python-list-data-structure-creation-access-8038ab0009fe?sk=3275d70fc17fc87a1b666001323c8831)를 편역한 것이다.
